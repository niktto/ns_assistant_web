import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

requires = [
    'pyramid',
    'SQLAlchemy',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'zope.sqlalchemy',
    'waitress',
    'wtforms',
    'webhelpers',
    'cryptacular'
    ]

setup(name='ns_assistant_web',
      version='0.0',
      description='ns_assistant_web',
      long_description='',
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='ns_assistant_web',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = ns_assistant_web:main
      [console_scripts]
      initialize_ns_assistant_web_db = ns_assistant_web.scripts.initializedb:main
      """,
)
