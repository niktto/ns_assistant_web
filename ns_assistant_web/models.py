# -*- coding: utf-8

import datetime
from cryptacular import pbkdf2
from sqlalchemy import ForeignKey, Table, MetaData
from sqlalchemy import (
    Column,
    Integer,
    Unicode,
    UnicodeText,
    DateTime
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

from .constants import STATS, SKILLS, BODY


players = Table('players', Base.metadata,
    Column('campaign_uid', Integer, ForeignKey('campaigns.uid')),
    Column('user_uid', Integer, ForeignKey('users.uid')),
)


class User(Base):

    __tablename__ = 'users'

    uid = Column(Integer, primary_key=True)
    name = Column(Unicode(255), unique=True)
    password_hash = Column(Unicode(255), nullable=False)
    campaigns = relationship('Campaign', secondary=players, backref="players")
    registered = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, name, password):
        self.name = name
        self.pwmanager = pbkdf2.PBKDF2PasswordManager()
        self.password_hash =self.pwmanager.encode(password)

    def check_password(self, password):
        return self.pwmanager.check(self.password_hash, password)


class Campaign(Base):

    __tablename__ = 'campaigns'

    uid = Column(Integer, primary_key=True)
    name = Column(Unicode(255), unique=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, name):
        self.name = name


class Session(Base):

    __tablename__ = 'sessions'

    uid = Column(Integer, primary_key=True)
    campaign = Column(Integer, ForeignKey('campaigns.uid'))
    name = Column(Unicode(255), unique=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, campaign, name):
        self.campaign = campaign
        self.name = name


class SessionLog(Base):

    __tablename__ = 'session_logs'

    uid = Column(Integer, primary_key=True)
    session = Column(Integer, ForeignKey('sessions.uid'))
    content = Column(UnicodeText)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, session, content):
        self.session = session
        self.content = content


#  ---- IN GAME OBJECTS BELOW ----


characters = Table('characters', Base.metadata,
    Column('uid', Integer, primary_key=True),
    Column('session', Integer, ForeignKey('sessions.uid')),
    Column('created', DateTime, default=datetime.datetime.utcnow),
    Column('name', Unicode(255)))

for part in BODY.keys():
    characters.append_column(Column(part, Integer))

for stat in STATS.keys():
    characters.append_column(Column('stat_' + stat, Integer))

for skill in SKILLS.keys():
    characters.append_column(Column('skill_' + skill, Integer))


#class Character(Base):
#    '''Skills and stats are static because I cant figure out how to dynamicaly
#    create columns in sqla'''
#
#    __tablename__ = 'characters'
#
#    uid = Column(Integer, primary_key=True)
#    session = Column(Integer, ForeignKey('sessions.uid'))
#    created = Column(DateTime, default=datetime.datetime.utcnow)
#
#    name = Column(Unicode(255))
#
#    #  Body health
#    for part in BODY.keys():
#        Column(part, Integer)
#
#    #  Stats
#    for stat in STATS.keys():
#        Column('stat_' + stat, Integer)
#
#    #  Skills
#    for skill in SKILLS.keys():
#        Column('skill_' + skill, Integer)
#
#    def __init__(self, stats_dict, skill_dict):
#        pass