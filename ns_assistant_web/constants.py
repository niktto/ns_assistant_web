# -*- coding: utf-8

STATS = {
    'budowa': u'Budowa',
    'zrecznosc': u'Zreczność',
    'percepcja': u'Percepcja',
    'charakter': u'Charakter',
    'spryt': u'Spryt'
}


SKILLS = {
    'bijatyka': u"Bijatyka",
    'bron_reczna': u"Broń Reczna",
    'rzucanie': u"Rzucanie",
    'pistolety': u"Pistolety",
    'karabiny': u"Karabiny",
    'bron_maszynowa': u"Broń Maszynowa",
    'wyrzutnie': u"Wyrzutnie",
    'mat_wybuchowe': u"Materiały Wybuchowe",
    'rusznikarstwo': u"Rusznikarstwo",
    'lucznictwo': u"Łuk",
    'kusze': u"Kusze",
    'proca': u"Proca",
    'skradanie': u"Skradanie Sie",
    'maskowanie': u"Maskowanie",
    'ukrywanie': u"Ukrywanie",
    'odpornosc': u"Odporność na Ból",
    'niezlomnosc': u"Niezłomność",
    'morale': u"Morale",
    'pierwsza_pomoc': u"Pierwsza Pomoc",
    'leczenie_ran': u"Leczenie Ran",
    'leczenie_chorob': u"Leczenie Chorób",
    'kondycja': u"Kondycja",
    'plywanie': u"Pływanie",
    'wspinaczka': u"Wspinaczka",
    'mechanika': u"Mechanika",
    'elektronika': u"Elektronika",
    'komputery': u"Komputery",
    'wypatrywanie': u"Wypatrywanie",
    'nasluchiwanie': u"Nasluchiwanie",
    'czujnosc': u"Czujność",
}


BODY = {
    'right_hand': u"Prawa reka",
    'left_hand': u"Lewa reka",
    'body': u'Korpus',
    'groin': u'Krocze',
    'right_leg': u'Prawa noga',
    'left_leg': u'Lewa noga',
    'head': u'Głowa',
}